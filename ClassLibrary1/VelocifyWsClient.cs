﻿using System;
using VelocifyWebService;

namespace VelocifyWs
{
  public class VelocifyWsClient {
    private ClientServiceSoapClient _client;

    /* Lead statuses currently in Velocify
        <option value="1">New</option>
		    <option value="2">Attempting Contact 1</option>
		    <option value="3">Attempting Contact 2</option>
		    <option value="4">Attempting Contact 3</option>
		    <option value="5">Attempting Contact 4</option>
		    <option value="6">Attempting Contact 5</option>
		    <option value="214">Nurture: Not Yet Contacted</option>
		    <option value="11">Contacted</option>
		    <option value="220">Email Sent (manual)</option>
		    <option value="208">Scheduled: Phone Interview</option>
		    <option value="211">Scheduled: In Person Interview</option>
		    <option value="212">Completed: Phone Interview</option>
		    <option value="213">Completed: In Person Interview</option>
		    <option value="210">Interview Rescheduled</option>
		    <option value="209">Interview No Show</option>
		    <option value="13">Application Process Started</option>
		    <option value="14">Application Submitted</option>
		    <option value="21">Temporary Out</option>
		    <option value="17">Permanent Out</option>
		    <option value="18">Nurturing</option>
		    <option value="19">Bad Lead</option>
		    <option value="20">DO NOT USE</option>
		    <option value="216">Email Only: No phone</option>
		    <option value="215">Interested: Still Taking PreReqs</option>
		    <option value="217">Transfer to LeadQual</option>
		    <option value="218">Successful Transfer to Agent</option>
		    <option value="219">Unsuccessful Transfer to Agent</option>
		    <option value="221">Cancel</option>
		    <option selected="selected" value="222">Do Not Call</option>
     */

    private Boolean modifyLeadStatus(int leadId, int statusId) {
      _client = new VelocifyWebService.ClientServiceSoapClient(ClientServiceSoapClient.EndpointConfiguration.ClientServiceSoap12);
      //System.Threading.Tasks.Task<System.Xml.Linq.XElement> result;
      string username = "lgabriel@parker.edu";
      string password = "Q@Vqav6yxZSF:8'$";
      var result = _client.ModifyLeadStatusAsync(username, password, leadId, statusId);
      int attempts = 0;
      while(!result.IsCompleted && attempts <= 10) {
        System.Threading.Thread.Sleep(1000);
        attempts++;
      }
      return true;
    }

    public Boolean leadRegistered(int leadId) => modifyLeadStatus(leadId, 17); // This actually changes the lead to "Permanent Out" for testing purposes - See velocify statuses comment above

  }
}
